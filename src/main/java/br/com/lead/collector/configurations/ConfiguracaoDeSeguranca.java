package br.com.lead.collector.configurations;

import br.com.lead.collector.security.FiltroDeAutenticacao;
import br.com.lead.collector.security.FiltrodeAutorizacao;
import br.com.lead.collector.security.JWTUtil;
import br.com.lead.collector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    //constante aonde irá armazenar as strings que querem autorizar
    private static final String[] PUBLIC_MATCHERS_GET = {
            "/leads",
            "/leads/*",
            "/produtos"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/leads",
            "/usuario",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //é atraves dele que configura acesso, etc

        //desabilitando esse preenchimento de token no formulario, dessa forme, toda e qualquer
        // post não retornará mais o 403.
        http.csrf().disable();
        http.cors();

        //está conficurando no authorizeRequest, aonde ocorrer o match, um métpdo get http na url /leads,
        // permite o acesso, para qualquer outra, pede autorização
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .anyRequest().authenticated();


        //pq RESTFul é stateless - não utiliza sessão no sisteema
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltrodeAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
    }

//    @CrossOrigin(origins = "http://123.78.000.762", value = "/leads")

    @Bean
    CorsConfigurationSource configuracaoDeCors() {
        //ta pedindo para ele registrar todos os end-points da API, podem receber requisições de outras fontes, outros servidores
        //então para todas as urls, está passando a configuração padrão

        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
