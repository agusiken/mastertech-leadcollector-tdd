package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    //fazemos apenas requisições falsas para o Controller, por isso o MockMvc
    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setNome("Usuario");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("usuario@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setIdProduto(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });
//
//        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);

        //converte json ou converte objeto
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        //responsavel pelas requisições falsas
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString()))
                );

    }

    public void testarAtualizarLeadComSucesso() throws Exception{
        lead.setId(1);
        lead.setData(LocalDate.now());
        Mockito.when(leadService.atualizarLead(Mockito.anyInt(), Mockito.any(Lead.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead)).andReturn();


    }
}
