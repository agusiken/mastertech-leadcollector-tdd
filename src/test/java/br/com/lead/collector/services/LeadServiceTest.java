package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Usuario");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("usuario@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setIdProduto(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarSalvarLead() {
        Mockito.when(produtoService.buscarPorTodosOsId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Andressa");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("andressa@hotmail.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(leadTeste)).then(leadLamb -> {
            //simula o preenchimento do id como se fosse o banco de dados;
                    leadTeste.setId(1);
                    return leadTeste;
                }
        );

//        Mockito.when(leadRepository.save(leadTeste)).thenReturn((leadTeste));

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(1, leadTeste.getId());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));
    }

    @Test
    public void testarBuscarPorTodosOsLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarAtualizarLead() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadTeste = new Lead();
        leadTeste.setId(1);
        leadTeste.setNome("Ayumi");
        leadTeste.setData(LocalDate.now());
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("ayumi@com.br");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);
        Lead leadAtualizado = leadService.atualizarLead(1, leadTeste);

        Assertions.assertEquals(leadAtualizado, leadTeste);
    }

    @Test
    public void testarDeletarLead() {
        //precisa verificar se o deletarLead tá chamando o repositorio com sucesso, se sim, ele está deletando
        boolean existeLead = true;
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(existeLead);

        leadService.deletarLead(1);

        //verifica se chamou o metodo se é void, e o 1 é quantas vezes o método será chamado
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());

        //Assertions.assertThrows(RuntimeException.class, () -> {leadService.deletarLead(8001);});

    }
}
